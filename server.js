const express = require('express')
const app = express()

const cors = require('cors')
app.use(cors())

const config = require('./config/index')
config()

const db = require('./utils/db')
const errorHandler = require('./middlewares/error')

const port = process.env.PORT || 8080

app.use(express.urlencoded({ extended: true }))
app.use(express.json())

// Add routers
const authRoutes = require('./routes/auth')
const prjRoutes = require('./routes/projects')
const resRoutes = require('./routes/resources')
const usrRoutes = require('./routes/users')

app.use('/api/v1/auth', authRoutes)
app.use('/api/v1/projects', prjRoutes)
app.use('/api/v1/resources', resRoutes)
app.use('/api/v1/users', usrRoutes)

app.use(errorHandler)

db.connect()
  .then((success) => {
    app.listen(port)
    console.log(`Server listening on port ${port}`)
  })
  .catch((err) => {
    console.log(err)
  })
