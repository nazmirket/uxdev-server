const UserService = require('../services/UserService')
const asyncHandler = require('../middlewares/async')

module.exports.create = asyncHandler(async (req, res, next) => {
  let user = await UserService.create(req.body)
  res.status(201).json({ success: true, user: user })
})

module.exports.get = asyncHandler(async (req, res, next) => {
  let givenId = req.params.id
  let userId = req.user.id

  if (givenId == userId.toString() || req.user.role == 'admin') {
    let user = await UserService.get(givenId)
    return res.status(200).json({ success: true, user: user })
  }

  res.status(403).json({
    success: false,
    error: 'Permission denied',
  })
})

module.exports.query = asyncHandler(async (req, res, next) => {
  let results = await UserService.query(req.query)
  res.status(200).json(results)
})

module.exports.resetPassword = asyncHandler(async (req, res, next) => {
  let token = req.body.token
  let password = req.body.password
})

module.exports.update = asyncHandler(async (req, res, next) => {})

module.exports.del = asyncHandler(async (req, res, next) => {
  let userId = req.params.id

  let user = await UserService.delete(userId)

  return res.status(200).json({
    success: true,
    user: user,
  })
})
