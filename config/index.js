const dotenv = require('dotenv')

module.exports = () => {
  dotenv.config({ path: './config/.env' })
  console.log('Environment variables are set...')
}
