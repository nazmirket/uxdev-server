const express = require('express')

const router = express.Router()

const resourceController = require('../controllers/resources')

let { protect, authorize } = require('../middlewares/auth')

let { create, get, update, del } = resourceController

router.get('/:id', protect, get)
router.post('/', protect, authorize('admin'), create)
router.put('/:id', protect, authorize('admin'), update)
router.delete('/:id', protect, authorize('admin'), del)

module.exports = router
