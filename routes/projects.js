const express = require('express')

const router = express.Router()

const projectController = require('../controllers/projects')

let { protect, authorize, check } = require('../middlewares/auth')

let {
  create,
  get,
  query,
  update,
  del,
  confirm,
  stats,
  reject,
} = projectController

router.put('/confirm/:id', protect, authorize('admin'), confirm)
router.put('/reject/:id', protect, authorize('admin'), reject)

router.get('/stats', protect, authorize('admin'), stats)

router.get('/', protect, authorize('admin'), query)
router.get('/:id', protect, get)

router.post('/', check, create)
router.put('/:id', protect, authorize('admin'), update)
router.delete('/:id', protect, authorize('admin'), del)

module.exports = router
