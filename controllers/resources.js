const ResourceService = require('../services/ResourceService')
const asyncHandler = require('../middlewares/async')

module.exports.create = asyncHandler(async (req, res, next) => {})

module.exports.get = asyncHandler(async (req, res, next) => {})

module.exports.update = asyncHandler(async (req, res, next) => {})

module.exports.del = asyncHandler(async (req, res, next) => {})
