const User = require('../models/User')
const ErrorResponse = require('../utils/ErrorResponse')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

module.exports.login = async (email, password) => {
  let user = await User.findOne({ email: email }, 'email name password role')

  if (!user) {
    throw new ErrorResponse('There is no user with the given email', 400)
  }

  let match = await bcrypt.compare(password, user.password)

  if (!match) {
    throw new ErrorResponse('Pasword is incorrect', 401)
  }

  const token = jwt.sign(
    {
      email: user.email,
      role: user.role,
      id: user._id,
    },
    process.env.JWT_SECRET,
    { expiresIn: '1d' }
  )

  return {
    user: user,
    token: token,
  }
}

module.exports.forgotPassword = (email) => {}
