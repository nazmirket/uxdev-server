const User = require('../models/User')
const ErrorResponse = require('../utils/ErrorResponse')
const queryBuilder = require('../helpers/query-builder')
const bcrypt = require('bcryptjs')
const generator = require('generate-password')

module.exports.create = async (props) => {
  let { name, email, password } = props

  let salt = await bcrypt.genSalt(10)
  let hash = await bcrypt.hash(password, salt)

  let user = new User({
    name,
    email,
    password: hash,
  })

  await user.save()

  return user
}

module.exports.generatePassword = (length) => {
  return generator.generate({
    length: length || 8,
    numbers: true,
    symbols: false,
    uppercase: true,
    lowercase: true,
  })
}

module.exports.resetPassword = async (user, password) => {
  let salt = await bcrypt.genSalt(10)
  let hash = await bcrypt.hash(password, salt)

  await user.update({ password: hash })
  return user
}

module.exports.checkUser = async (email) => {
  return await User.findOne({ email: email })
}

module.exports.get = async (id) => {
  let user = await User.findById(id).populate({
    path: 'projects',
    select: 'title description price finished confirmed madeWith',
  })

  if (!user) {
    throw new ErrorResponse(`User with id ${id} not found`, 404)
  }
  return user
}

module.exports.update = async (id, props) => {
  let user = await User.findByIdAndUpdate({ _id: id }, props)
  if (!user) {
    throw new ErrorResponse(`User with id ${id} not found and updated`, 404)
  }
  return user
}

module.exports.delete = async (id) => {
  let user = await User.findById(id)
  await user.remove()
  if (!user) {
    throw new ErrorResponse(`User with id ${id} not found and deleted`, 404)
  }
  return user
}

module.exports.query = async (queryParams) => {
  let users = queryBuilder(queryParams, User, {
    path: 'projects',
    select: 'title description price finished confirmed madeWith',
  })
  return users
}
