const jwt = require('jsonwebtoken')
const asyncHandler = require('./async')
const ErrorResponse = require('../utils/ErrorResponse')
const UserService = require('../services/UserService')

// Decode token and attach user to the req object
exports.protect = asyncHandler(async (req, res, next) => {
  let header = req.get('Authorization')

  if (!header) {
    throw new ErrorResponse('Cannot find authorization header', 401)
  }

  let token = header.startsWith('Bearer') ? header.split(' ')[1] : null

  if (!token) {
    throw new ErrorResponse('Not authorized to access this route', 401)
  }

  const decoded = jwt.verify(token, process.env.JWT_SECRET)

  req.user = await UserService.get(decoded.id)

  next()
})

exports.check = asyncHandler(async (req, res, next) => {
  let header = req.get('Authorization')

  if (!header) {
    return next()
  }

  let token = header.startsWith('Bearer') ? header.split(' ')[1] : null

  if (!token) {
    return next()
  }

  const decoded = jwt.verify(token, process.env.JWT_SECRET)
  req.user = await UserService.get(decoded.id)

  next()
})

// Check user role for the requested route
exports.authorize = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(
        new ErrorResponse(
          `User role ${req.user.role} is not authorized to access this route`,
          403
        )
      )
    }
    next()
  }
}
