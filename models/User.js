const mongoose = require('mongoose')

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Name can't be empty"],
      minlength: 3,
      maxlength: 20,
    },
    email: {
      type: String,
      unique: true,
      required: [true, "Email can't be empty"],
    },
    password: {
      type: String,
      required: [true, 'Password should be given'],
      select: false,
    },
    role: {
      type: String,
      required: false,
      default: 'user',
    },
    confirmed: {
      type: Boolean,
      required: true,
      default: false,
    },
    job: {
      type: String,
    },
    company: {
      type: String,
    },
    gender: {
      type: String,
    },
    avatar: {
      type: String,
      required: false,
    },
    adress: {
      city: {
        type: String,
      },
      country: {
        type: String,
      },
      street: {
        type: String,
      },
      zipcode: {
        type: Number,
      },
    },
    resetPasswordToken: {
      type: String,
    },
    resetPasswordExpire: {
      type: Date,
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
)

schema.virtual('projects', {
  ref: 'Project',
  localField: '_id',
  foreignField: 'owner',
  justOne: false,
})

schema.pre('remove', async function (next) {
  await this.model('Project').deleteMany({ owner: this._id })
  next()
})

module.exports = mongoose.model('User', schema)
